{ MIT License

  Copyright (c) 2018-2024 Yevhen Loza

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
}

{----------------------------------------------------------------------------}

(* CastleCraft is an extremely simple Infiminer clone,
   aimed at showning how Infiminer-like visualization may be made in
   Castle Game Engine quick and easily.
   This is not a full-scale game,
   you can generate a map, dig holes, walk, jump and build.

   Game controls:
   To dig use Left mouse button
   To build use Right mouse button
   WASD to walk
   Space to jump *)

unit GameViewMain;

interface

uses Classes,
  CastleVectors, CastleComponentSerialize, X3DNodes, CastleScene,
  CastleUIControls, CastleControls, CastleKeysMouse, CastleCameras, CastleViewport;

const
  { This is XYZ size of a map Chunk.
    In a normal Infiminer clone we would use multiple chunks 16x16x16
    However, that would require a more complex chunk manager, which would
    complicate the code without any serious gain for this demonstration }
  MaxX = 64;
  MaxY = MaxX;
  MaxZ = MaxX;

const
  { how far can player reach to operate (build/destroy) blocks }
  MaxDrillDistance = 4;

type
  { This is a chunk of map
    Caution. It is not intended that this chunk would be docked to other
    map chunks, so we omit docking issues for simplicity }
  TMap = array[0..MaxX, 0..MaxY, 0..MaxZ] of Integer;

type
  { Main view, where most of the application logic takes place. }
  TViewMain = class(TCastleView)
  strict private
    { Map, consisting of blocks }
    Map: TMap;

    { Nodes used in the Scene generation }
    Appearance: TAppearanceNode;    // contains material
    Material: TMaterialNode;        // colored material
    { for efficniency reasons we're using all 16 textures of the blocks
      packed up in a single Texture, also we set "NEAREST" filter
      in Texture.TextureProperties to make them look pixelart-like }
    Texture: TImageTextureNode;

    { Larger game logic entities }
    Scene: TCastleScene;   // Main scene, containing all the world and player's light
    { The core of the algorithm
      Rebuilds the Scene based on Map
      Should be called every time the Map has been changed }
    procedure RebuildScene;
    { Generate a simple random map of blocks }
    procedure GenerateMap;
    { Initializes all the nodes that are reused throughout the game }
    procedure InitAllNodes;
    { Try drilling a block player is pointing at }
    procedure Drill;
    { Try building at a block player is pointing at }
    procedure Build;
    { Get coordinates of a block the player is looking at
      (based on Camera.Position and Camera.Direction)
      returns true if the block is valid (e.g. within reach)
      PointAtBlocked = true returns coordinated of the first blocked block encountered
      PointAtBlocked = false returns coordinates of the last free block encountered }
    function GetMouseImpact(PointAtBlocked: Boolean; out BlockCoord: TVector3Integer): Boolean;
  published
    { Components designed using CGE editor.
      These fields will be automatically initialized at Start. }
    LabelFps: TCastleLabel;
    Viewport: TCastleViewport;
    WalkNavigation: TCastleWalkNavigation;       // Player entity, used for Camera and controls
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewMain: TViewMain;

implementation

uses
  SysUtils, CastleWindow, CastleTextureImages,
  CastleFilesUtils, CastleRandom;

procedure TViewMain.RebuildScene;
var
  Coords: TCoordinateNode;        // contains x,y,z of all vertexes
  TextureCoords: TTextureCoordinateNode; //contains texture coordinates, must be in sync with Coords
  Geometry: TIndexedFaceSetNode;  // face set of each wall&floor
  Shape: TShapeNode;              // contains the coords and geometry
  Root: TX3DRootNode;             // contains the Shape and all other nodes (including navigation)

  { Cached indexes of the cubes corners coordinates and texture coordinates }
  Indexes: TMap;
  Index: Integer; //last index of geometry
  TexIndex: Integer; //last index of texture coordinates

  { Clears all indexes at the Map and starts over indexing }
  procedure ClearIndexes;
  var
    IX, IY, IZ: Integer;
  begin
    Index := -1;
    for IX := 0 to MaxX do
      for IY := 0 to MaxY do
        for IZ := 0 to MaxZ do
          Indexes[IX, IY, IZ] := -1;
  end;

  { The core procedure that fills in corresponding values for
    TCoordinateNode, TTextureCoordinateNode and TIndexedFaceSetNode
    for all the Map }
  procedure CraftTheWorld;
  var
    IX, IY, IZ: Integer;
    { Adds a geometry point based on shift DX, DY, DZ from initial IX, IY, IZ }
    procedure AddPoint(DX, DY, DZ: Integer);
    begin
      if Indexes[IX + DX, IY + DY, IZ + DZ] = - 1 then
      begin
        { if Index is not cached we need to create a new Coords.FdPoint }
        Coords.FdPoint.Items.Add(Vector3((IX + DX), (IY + DY), (IZ + DZ)));
        Inc(Index);
        Geometry.FdCoordIndex.Items.Add(Index);
        { and cache the index }
        Indexes[IX + DX, IY + DY, IZ + DZ] := Index;
      end else
        { otherwise, jsut add the cached index to Geometry }
        Geometry.FdCoordIndex.Items.Add(Indexes[IX + DX, IY + DY, IZ + DZ]);
    end;

    { Adds texture coordinates for each face }
    procedure AddTex(AValue: Integer);
    const
      { texture coordinates of corners }
      CX: array[0..3] of Integer = (0, 1, 1, 0);
      CY: array[0..3] of Integer = (0, 0, 1, 1);
      { texture coordinates of different textures in a "global" 4x4 texture }
      V: array[1..16] of TVector2 =
        (
        (X: 0/4; Y: 0/4),
        (X: 1/4; Y: 0/4),
        (X: 2/4; Y: 0/4),
        (X: 3/4; Y: 0/4),
        (X: 0/4; Y: 1/4),
        (X: 1/4; Y: 1/4),
        (X: 2/4; Y: 1/4),
        (X: 3/4; Y: 1/4),
        (X: 0/4; Y: 2/4),
        (X: 1/4; Y: 2/4),
        (X: 2/4; Y: 2/4),
        (X: 3/4; Y: 2/4),
        (X: 0/4; Y: 3/4),
        (X: 1/4; Y: 3/4),
        (X: 2/4; Y: 3/4),
        (X: 3/4; Y: 3/4)
        );
    var
      Corner: Integer;
    begin
      { Just cycle through all the corners and add the correct texture coordinates
        We don't care about proper texture orientation here
        and give the same orientation for all faces }
      for Corner := 0 to 3 do
      begin
        TextureCoords.FdPoint.Items.Add(Vector2(CX[Corner] / 4 + V[AValue].X, CY[Corner] / 4 + V[AValue].Y));
        Inc(TexIndex);
        Geometry.FdTexCoordIndex.Items.Add(TexIndex);
      end;
      { and make a "stop" }
      Geometry.FdTexCoordIndex.Items.Add(-1);
    end;

    { The following set of procedures creates quads on different sides of the block
      the only important thing here to do is to provide valid indexes to
      Geometry.FdCoordIndex so that the points of the Geometry would be
      enumerated counter-clockwise as seen from the empty block side
      So, we just add points and index them and add a stop-index "-1" to finish
      the quad. Finally we add a texture to it according to the block type. }
    procedure FrontQuad(AValue: Integer);
    begin
      if AValue = 0 then
        Exit;
      AddPoint(1, 0, 0);
      AddPoint(1, 0, 1);
      AddPoint(1, 1, 1);
      AddPoint(1, 1, 0);
      Geometry.FdCoordIndex.Items.Add(-1);
      AddTex(AValue);
    end;
    procedure BackQuad(AValue: Integer);
    begin
      if AValue = 0 then
        Exit;
      AddPoint(0, 0, 0);
      AddPoint(0, 1, 0);
      AddPoint(0, 1, 1);
      AddPoint(0, 0, 1);
      Geometry.FdCoordIndex.Items.Add(-1);
      AddTex(AValue);
    end;
    procedure TopQuad(AValue: Integer);
    begin
      if AValue = 0 then
        Exit;
      AddPoint(0, 1, 0);
      AddPoint(1, 1, 0);
      AddPoint(1, 1, 1);
      AddPoint(0, 1, 1);
      Geometry.FdCoordIndex.Items.Add(-1);
      AddTex(AValue);
    end;
    procedure BottomQuad(AValue: Integer);
    begin
      if AValue = 0 then
        Exit;
      AddPoint(0, 0, 0);
      AddPoint(0, 0, 1);
      AddPoint(1, 0, 1);
      AddPoint(1, 0, 0);
      Geometry.FdCoordIndex.Items.Add(-1);
      AddTex(AValue);
    end;
    procedure LeftQuad(AValue: Integer);
    begin
      if AValue = 0 then
        Exit;
      AddPoint(0, 0, 0);
      AddPoint(1, 0, 0);
      AddPoint(1, 1, 0);
      AddPoint(0, 1, 0);
      Geometry.FdCoordIndex.Items.Add(-1);
      AddTex(AValue);
    end;
    procedure RightQuad(AValue: Integer);
    begin
      if AValue = 0 then
        Exit;
      AddPoint(0, 0, 1);
      AddPoint(0, 1, 1);
      AddPoint(1, 1, 1);
      AddPoint(1, 0, 1);
      Geometry.FdCoordIndex.Items.Add(-1);
      AddTex(AValue);
    end;

    begin
      { Clear all indexes }
      TexIndex := -1;
      ClearIndexes;

      { Next we enumerate all empty blocks on the map
        if the block is empty, try adding top/front/back... quads to it
        (so, the quads are only created where there is empty space nearby
        i.e. we won't need any quads for solid rock)
        the procedures will automatically cancel adding a quad
        if the adjacent block is empty, so we don't check it here.
        Note Y-up, X-forward orientation which is default in Castle Game Engine }
      for IX := 1 to Pred(MaxX) do
        for IY := 1 to Pred(MaxY) do
          for IZ := 1 to Pred(MaxZ) do
            if Map[IX, IY, IZ] = 0 then
            begin
              FrontQuad(Map[IX + 1, IY, IZ]);
              BackQuad(Map[IX - 1, IY, IZ]);
              TopQuad(Map[IX, IY + 1, IZ]);
              BottomQuad(Map[IX, IY - 1, IZ]);
              RightQuad(Map[IX, IY, IZ + 1]);
              LeftQuad(Map[IX, IY, IZ - 1]);
            end;
  end;

begin
  Scene.Clear;

  { Re-create some nodes, actually those may be reused to speed the things up,
    but some bugs do occur and it's simpler just to create them again every time
    Pay attention, no freeing is required,
    as the nodes are reference-counted and are automatically freed }
  Coords := TCoordinateNode.Create;
  TextureCoords := TTextureCoordinateNode.Create;
  Geometry := TIndexedFaceSetNode.Create;
  Geometry.Coord := Coords;
  Geometry.TexCoord := TextureCoords;

  { Prepeare the World Geometry}
  CraftTheWorld;

  { and do some final manipulations - pack the created Geometry into TShapeNode
    and then into TX3DRootNode together with player navigation node }
  Shape := TShapeNode.Create;
  Shape.Appearance := Appearance;
  Shape.Geometry := Geometry;

  Root := TX3DRootNode.Create;
  Root.AddChildren(Shape);

  { Finally reinitialize the Scene with the new Root node }
  Scene.Load(Root, true);
end;

procedure TViewMain.GenerateMap;
const
  { size of the starting area = 2*StartAreaSize+1 }
  StartAreaSize = 2;
var
  IX, IY, IZ: Integer;
begin
  for IX := 0 to MaxX do
    for IY := 0 to MaxY do
      for IZ := 0 to MaxZ do
      begin
        { Border cubes are type 16 }
        if (IX = 0) or (IY = 0) or (IZ = 0) or
           (IX = MaxX) or (IY = MaxY) or (IZ = MaxZ) then
          Map[IX, IY, IZ] := 16
        else
        { inner cubes may be of 1..15 type }
          Map[IX, IY, IZ] := (1 + Round(14 * Sqr(Sqr(Sqr(Sqr(Rand.Random))))));
      end;

  { generate a free area for the player to start }
  for IX := MaxX div 2 - StartAreaSize to MaxX div 2 + StartAreaSize do
    for IY := MaxY div 2 - StartAreaSize to MaxY div 2 + StartAreaSize do
      for IZ := MaxZ div 2 - StartAreaSize to MaxZ div 2 + StartAreaSize do
        Map[IX, IY, IZ] := 0;
end;

procedure TViewMain.InitAllNodes;
begin
  { Create a diffuse material }
  Material := TMaterialNode.Create;
  Material.DiffuseColor := Vector3(1, 1, 1);
  Material.AmbientIntensity := 0;

  { Load a texture from a .png file }
  Texture := TImageTextureNode.Create;
  Texture.SetUrl([ApplicationData('texture_CC0_by_Dungeon_Crawl_Stone_Soup_team.png')]);

  { Make texture pixelart-like, i.e. turn off anisotropic smoothing }
  Texture.TextureProperties := TTexturePropertiesNode.Create;
  Texture.TextureProperties.AnisotropicDegree := 0;

  { And finally pack everything into TAppearanceNode }
  Appearance := TAppearanceNode.Create;
  Appearance.Material := Material;
  Appearance.Texture := Texture;
end;

procedure TViewMain.Drill;
var
  PointAt: TVector3Integer;
begin
  if GetMouseImpact(true, PointAt) then
    Map[PointAt[0], PointAt[1], PointAt[2]] := 0;
  RebuildScene;
end;

procedure TViewMain.Build;
var
  PointAt: TVector3Integer;
begin
  if GetMouseImpact(false, PointAt) then
    Map[PointAt[0], PointAt[1], PointAt[2]] := 1;
  RebuildScene;
end;

function TViewMain.GetMouseImpact(PointAtBlocked: Boolean; out
  BlockCoord: TVector3Integer): Boolean;
var
  { initial and current coordinates of the ray}
  X0, X1: TVector3Integer;
  { current ray length }
  R: Single;

  { converts a Float TVector3 into TVector3Integer by truncating each coordinate
    practically it is obtaining a block coordinates based on float coordinates}
  function TruncVec(AVector: TVector3): TVector3Integer;
  begin
    Result.X := Trunc(AVector.X);
    Result.Y := Trunc(AVector.Y);
    Result.Z := Trunc(AVector.Z);
  end;

  { returns True if this block is border block }
  function IsBorder(APosition: TVector3Integer): Boolean;
  begin
    Result :=
      ((APosition[0] <= 0) or (APosition[1] <= 0) or (APosition[2] <= 0) or
       (APosition[0] >= MaxX) or (APosition[1] >= MaxY) or (APosition[2] >= MaxZ));
  end;

  { is the block at APosition free? }
  function MapFree(APosition: TVector3Integer): Boolean;
  begin
    if IsBorder(APosition) then
      Result := false
    else
      Result := Map[APosition[0], APosition[1], APosition[2]] = 0;
  end;

begin
  { initialize with starting location }
  X0 := TruncVec(WalkNavigation.Camera.Translation);
  R := 0;
  { now do a very wrong and inefficient raycast, just dirty and simple }
  repeat
    { get coordinates of next block }
    R += 0.1;
    X1 := TruncVec(WalkNavigation.Camera.Translation + R * WalkNavigation.Camera.Direction);

    if not MapFree(X1) then
    begin
      { if this block is blocked... }
      Result := true;
      if PointAtBlocked then
      begin
        if IsBorder(X1) then
          { border blocks can't be mined }
          Result := false
        else
          { otherwise return X1 to mine }
          BlockCoord := X1
      end else
        if not TVector3Integer.Equals(X0, TruncVec(WalkNavigation.Camera.Translation)) then
          { return last free block to build }
          BlockCoord := X0
        else
          { can't build at point where the player's camera is located }
          Result := false;
      { and stop the raycasting }
      Exit;
    end;

    X0 := X1;
  until R > MaxDrillDistance;
  { if no minable block is encountered within player's reach, exit }
  Result := false;
end;

constructor TViewMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/gameviewmain.castle-user-interface';
end;

procedure TViewMain.Start;
begin
  inherited;
  // Generate a map
  GenerateMap;
  // Create Scene
  Scene := TCastleScene.Create(Application);
  Scene.PreciseCollisions := true;
  Scene.ProcessEvents := false;
  Scene.RenderOptions.PhongShading := true;
  // Use pixelart-like filters (nearest)
  Scene.RenderOptions.MagnificationFilter := magNearest;
  Scene.RenderOptions.MinificationFilter := minNearest;
  // Init player position and jump height
  Viewport.Camera.Translation := Vector3(MaxX / 2, MaxY / 2, MaxZ / 2);
  WalkNavigation.JumpMaxHeight := 0.6;
  // Create reusable nodes
  InitAllNodes;

  // and build the initial scnee
  RebuildScene;

  // Load Scene into Viewport
  Viewport.Items.Add(Scene);
end;

procedure TViewMain.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  LabelFps.Caption := 'FPS: ' + Container.Fps.ToString;
end;

function TViewMain.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  if Result then Exit; // allow the ancestor to handle keys

  if Event.EventType = itMouseButton then
  begin
    if Event.IsMouseButton(buttonLeft) then
      { left button is drilling }
      Drill
    else
    if Event.IsMouseButton(buttonRight) then
      { right button is building }
      Build;
  end;
end;

end.
